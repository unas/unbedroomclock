import time
from datetime import datetime
import RPi.GPIO as GPIO
import sys
import threading
import logging

class NightTime:
	minTime = ["6:00", "6:00", "6:00", "6:00", "6:00", "6:00", "6:00"]
	maxTime = ["21:00", "21:00", "21:00", "21:00", "22:30", "22:30", "21:00"]

logging.basicConfig(filename='clock.log',level=logging.INFO,format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

LD1 = 10
LD2 = 9
LD3 = 11
LD4 = 5

LEDA = 4
LEDB = 17
LEDC = 27
LEDD = 22

def Init():
	logging.info("Initializing...")
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(LEDA, GPIO.OUT)
	GPIO.setup(LEDB, GPIO.OUT)
	GPIO.setup(LEDC, GPIO.OUT)
	GPIO.setup(LEDD, GPIO.OUT)
	GPIO.setup(LD1, GPIO.OUT)
	GPIO.setup(LD2, GPIO.OUT)
	GPIO.setup(LD3, GPIO.OUT)
	GPIO.setup(LD4, GPIO.OUT)
	
	# First turning all LDs on, so that the digit's can be zeroed
	GPIO.output(LD1, 1)
	GPIO.output(LD2, 1)
	GPIO.output(LD3, 1)
	GPIO.output(LD4, 1)
	
	# Putting zero on all LEDs
	GPIO.output(LEDA, 0)
	GPIO.output(LEDB, 0)
	GPIO.output(LEDC, 0)
	GPIO.output(LEDD, 0)
	
	# Turning LDs back off
	GPIO.output(LD1, 0)
	GPIO.output(LD2, 0)
	GPIO.output(LD3, 0)
	GPIO.output(LD4, 0)
	
	logging.info("initialization complete")

def CloseLED(LED):
	# The led can be closed by setting a binary value larger than 1001
	# Setting value 1111
	GPIO.output(LEDA, 1)
	GPIO.output(LEDB, 1)
	GPIO.output(LEDC, 1)
	GPIO.output(LEDD, 1)
	if (LED == 1):
		# Turning LD momentary on
		GPIO.output(LD1, 1)
		#Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD momentary on
		GPIO.output(LD2, 1)
		#Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD momentary on
		GPIO.output(LD3, 1)
		#Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 4):
		# Turning LD momentary on
		GPIO.output(LD4, 1)
		#Turning LD back off
		GPIO.output(LD4, 0)

# Converts decimal number to binary number
def IntToBinArray(value):
	returnValue = [0, 0, 0, 0]
	if (value > 9):
		return returnValue

	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 3 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr

	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i])

	return returnValue

# Set's a specific LED's number
def SetLEDValue(LED, number):
	# Converting number to binary array
	binArray = IntToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		logging.error("Error, number is not 4-bit")
		return 0
	
	GPIO.output(LEDD, binArray[0]) # D
	GPIO.output(LEDC, binArray[1]) # C
	GPIO.output(LEDB, binArray[2]) # B
	GPIO.output(LEDA, binArray[3]) # A
	
	if (LED == 1):
		# Turning LD on
		GPIO.output(LD1, 1)
		# Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD on
		GPIO.output(LD2, 1)
		# Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD on
		GPIO.output(LD3, 1)
		# Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 4):
		# Turning LD on
		GPIO.output(LD4, 1)
		# Turning LD back off
		GPIO.output(LD4, 0)

def turnLEDSOff():
	GPIO.output(LEDD, 1) # D
	GPIO.output(LEDC, 1) # C
	GPIO.output(LEDB, 1) # B
	GPIO.output(LEDA, 1) # A
	
	GPIO.output(LD1, 1)
	GPIO.output(LD1, 0)
	GPIO.output(LD2, 1)
	GPIO.output(LD2, 0)
	GPIO.output(LD3, 1)
	GPIO.output(LD3, 0)
	GPIO.output(LD4, 1)
	GPIO.output(LD4, 0)

def isNightTime():
	weekday = datetime.now().weekday()
	now = datetime.strptime(str(datetime.now().hour) + ":" + str(datetime.now().minute), "%H:%M")
	minTime = datetime.strptime(NightTime.minTime[weekday], "%H:%M")
	maxTime = datetime.strptime(NightTime.maxTime[weekday], "%H:%M")
	
	return now < minTime or now >= maxTime

def main(argv):
	success = True
	try:
		logging.info("Starting clock program....")
		# Checking if debug is on
		debug = False
		for i in range(0, len(argv)):
			if (argv[i] == '-d' or argv[i] == '--debug'):
				debug = True
	
		# Initializing pins
		Init()
		
		logging.info("Starting main loop")
		
		# Starting main loop
		while True:
			# Setting current time to the screen
			hour = str(datetime.now().time().hour)
			minute = str(datetime.now().time().minute)
			timeArray = [0, 0, 0, 0]
			# Hour and minute length must be 2, so hour 4 -> 04
			if (len(hour) < 2):
				hour = '0' + hour
			if (len(minute) < 2):
				minute = '0' + minute
			
			currentTime = hour + minute
			for i in range(4):
				# If night time, closing leds
				if (isNightTime()):
					turnLEDSOff()
				else:
					SetLEDValue(i + 1, int(currentTime[i]))
			
			#Sleeping until the next minute
			time.sleep(60 - datetime.now().time().second)
			# Checking that we truly woke up when the minute changed, so that we won't accidentally skip a minute
			newMinute = datetime.now().time().minute
			while (newMinute == minute): # The minute hasn't changed for some reason (sleep woke too early?), sleeping some more
				logging.info("Had to adjust time, minute didn't change after sleep. Sleeping some more")
				extraSleepTime = 60 - datetime.now().time().second
				if (extraSleepTime == 0):
					extraSleepTime = 1
				time.sleep(extraSleepTime)
				newMinute = datetime.now().time().minute
			
		
	except Exception,e:
		logging.error("Error in main loop: " + str(e))
		success = False
	finally:
		logging.info("Program shutting down...")
		logging.info("Cleaning GPIO")
		GPIO.cleanup()

		# Returning value. If exiting with an error, then the main program will be restarted
		return success

if __name__ == "__main__": # Checking if this is the main program, not an imported module
	while True:
		returnValue = main(sys.argv)
		if (returnValue == True): # Exiting only if main didn't stop due to an error
			logging.info("Clean exit, shutting program down...")
			break
		else: # Main crashed due to an error. This is propably because an I/O error. This error is usually fixed by itself. Waiting 1 second and then starting main again
			logging.error("Error, restarting in 1 second...")
			time.sleep(1)