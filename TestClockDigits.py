import time
from datetime import datetime
import RPi.GPIO as GPIO
import sys


LD1 = 10
LD2 = 9
LD3 = 11
LD4 = 5

LEDA = 4
LEDB = 17
LEDC = 27
LEDD = 22

def Init():
	print("Initializing...")
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(LEDA, GPIO.OUT)
	GPIO.setup(LEDB, GPIO.OUT)
	GPIO.setup(LEDC, GPIO.OUT)
	GPIO.setup(LEDD, GPIO.OUT)
	GPIO.setup(LD1, GPIO.OUT)
	GPIO.setup(LD2, GPIO.OUT)
	GPIO.setup(LD3, GPIO.OUT)
	GPIO.setup(LD4, GPIO.OUT)

	# First turning all LDs on, so that the digit's can be zeroed
	GPIO.output(LD1, 1)
	GPIO.output(LD2, 1)
	GPIO.output(LD3, 1)
	GPIO.output(LD4, 1)

	# Putting zero on all LEDs
	GPIO.output(LEDA, 0)
	GPIO.output(LEDB, 0)
	GPIO.output(LEDC, 0)
	GPIO.output(LEDD, 0)

	# Turning LDs back off
	GPIO.output(LD1, 0)
	GPIO.output(LD2, 0)
	GPIO.output(LD3, 0)
	GPIO.output(LD4, 0)
	
	print("initialization complete")

def CloseLED(LED):
	# The led can be closed by setting a binary value larger than 1001
	# Setting value 1111
	GPIO.output(LEDA, 1)
	GPIO.output(LEDB, 1)
	GPIO.output(LEDC, 1)
	GPIO.output(LEDD, 1)
	if (LED == 1):
		# Turning LD momentary on
		GPIO.output(LD1, 1)
		#Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD momentary on
		GPIO.output(LD2, 1)
		#Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD momentary on
		GPIO.output(LD3, 1)
		#Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 4):
		# Turning LD momentary on
		GPIO.output(LD4, 1)
		#Turning LD back off
		GPIO.output(LD4, 0)

# Converts decimal number to binary number
def IntToBinArray(value):
	returnValue = [0, 0, 0, 0]
	if (value > 9):
		return returnValue
	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 3 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr
	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i])
	return returnValue

# Set's a specific LED's number
def SetLEDValue(LED, number):
	# Converting number to binary array
	binArray = IntToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		logging.error("Error, number is not 4-bit")
		return 0
	GPIO.output(LEDD, binArray[0]) # D
	GPIO.output(LEDC, binArray[1]) # C
	GPIO.output(LEDB, binArray[2]) # B
	GPIO.output(LEDA, binArray[3]) # A
	
	if (LED == 1):
		# Turning LD on
		GPIO.output(LD1, 1)
		# Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD on
		GPIO.output(LD2, 1)
		# Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD on
		GPIO.output(LD3, 1)
		# Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 4):
		# Turning LD on
		GPIO.output(LD4, 1)
		# Turning LD back off
		GPIO.output(LD4, 0)

def main(argv):
	try:
		# Initializing pins
		Init()
		
		print("Starting in...")
		for i in range(10, -1, -1):
			print(i)
			SetLEDValue(1, i)
			SetLEDValue(2, i)
			SetLEDValue(3, i)
			SetLEDValue(4, i)
			time.sleep(1)
		
		while (True):
			print("Counting one at a time")
			CloseLED(1)
			CloseLED(2)
			CloseLED(3)
			CloseLED(4)
			
			for i in range(4, 0, -1):
				for j in range(0, 10):
					SetLEDValue(i, j)
					time.sleep(0.5)
				CloseLED(i)
	finally:
		CloseLED(1)
		CloseLED(2)
		CloseLED(3)
		CloseLED(4)
		GPIO.cleanup()
		
	
if __name__ == "__main__": # Checking if this is the main program, not an imported module
    main(sys.argv)
